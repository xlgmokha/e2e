# frozen_string_literal: true

RSpec.shared_examples "with expected" do |language, package_manager, version, branch = 'master'|
  context "when generating a report for #{package_manager}" do
    let(:git_url) { "https://gitlab.com/gitlab-org/security-products/tests/#{language}-#{package_manager}.git" }
    let(:git_branch) { branch }

    pending { expect(subject).to match_schema(report_type) }

    pending do
      actual = JSON.pretty_generate(subject.to_h)
      expected_content = JSON.parse(
        fixture_file_content("expected/#{language}/#{package_manager}/#{branch}/v#{version}.json")
      )
      expected = JSON.pretty_generate(expected_content)

      expect(actual).to eq(expected)
    end
  end
end
