# frozen_string_literal: true

RSpec.shared_context 'with scanner' do
  subject { project.report_for(type: report_type) }

  let(:docker) { Docker.new(pwd: Pathname.pwd.join("src/#{scanner}")) }
  let(:docker_image) { "#{scanner}:latest" }
  let(:scanner) { raise "`scanner` not specified. Choose: #{Pathname.pwd.glob('src/*').map(&:basename).join(', ')}" }
  let(:project) { Project.new }
  let(:project_fixture) { nil }
  let(:env) { {} }
  let(:report_types) { { 'gemnasium-maven' => :dependency_scanning, 'spotbugs' => :sast } }
  let(:report_type) { report_types.fetch(scanner) }

  around do |example|
    if project_fixture
      project.mount(dir: fixture_file(project_fixture))
    else
      project.clone(git_url, branch: git_branch.to_s)
    end

    docker.run(image: docker_image, project_path: project.path, env: env)
    example.run
    project.cleanup
  end
end

RSpec.configure do |config|
  config.include_context 'with scanner'
end
