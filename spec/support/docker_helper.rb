# frozen_string_literal: true
RSpec.configure do |config|
  config.before(:suite) do
    Pathname.pwd.join('src').each_child do |file|
      next unless file.directory?

      Docker
        .new(pwd: file)
        .build(tag: "#{file.basename}:latest")
    end
  end
end
