# frozen_string_literal: true

RSpec.describe 'spotbugs' do
  let(:project_fixture) { 'java/maven/custom-tls' }
  let(:scanner) { 'spotbugs' }

  pending { expect(subject).to match_schema(:sast) }
  specify { expect(subject['version']).to eql('3.0') }
end
