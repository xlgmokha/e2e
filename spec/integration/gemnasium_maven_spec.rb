# frozen_string_literal: true

RSpec.describe 'gemnasium-maven' do
  let(:scanner) { 'gemnasium-maven' }

  %w[8 13 11 14].each do |java_version|
    context "when scanning a #{java_version} project with a custom x509 certificate" do
      let(:project_fixture) { 'java/maven/custom-tls' }
      let(:env) { { 'ADDITIONAL_CA_CERT_BUNDLE' => X509.self_signed.to_pem, 'DS_JAVA_VERSION' => java_version } }

      pending { expect(subject).to match_schema(:dependency_scanning) }

      specify do
        files = subject.to_h['dependency_files']
        expect(files.count).to be(1)
        expect(files[0]['dependencies'].count).to be(1)
        expect(files[0]['dependencies'][0]['package']['name']).to eql('com.fasterxml.jackson.core/jackson-core')
        expect(files[0]['dependencies'][0]['version']).to eql('2.10.0')
      end
    end
  end

  it_behaves_like "with expected", :java, :maven, '2.3', :master
end
