# frozen_string_literal: true

class Docker
  DEFAULT_ENV = {
    'CI_DEBUG_TRACE' => 'true',
    'CI_PROJECT_DIR' => '/tmp/app',
    'SECURE_LOG_LEVEL' => 'debug'
  }.freeze
  attr_reader :pwd

  def initialize(pwd: Pathname.pwd)
    @pwd = pwd
  end

  def build(tag:)
    Dir.chdir pwd do
      env = { 'DOCKER_BUILDKIT' => '1' }
      system(env, 'docker', 'build', "--network=host", "-t", tag, ".", exception: true)
    end
  end

  def run(image:, project_path: Pathname.pwd, env: {}, debug: ENV.fetch('DEBUG', 'false') == 'true')
    env_options = DEFAULT_ENV.merge(env).map { |(key, value)| "--env #{key}='#{value}'" }
    Dir.chdir pwd do
      arguments = [
        :docker, :run, '--rm',
        "--volume=#{project_path}:/tmp/app",
        '--network=host',
        '--add-host=maven.test:127.0.0.1',
        env_options
      ]
      arguments.push(debug ? ["-it", "--entrypoint=''", image, '/bin/bash -l'] : [image, '/analyzer run'])
      command = expand(arguments)
      system(command, exception: true)
    end
  end

  private

  def expand(command)
    command.flatten.map(&:to_s).join(' ')
  end
end
