# frozen_string_literal: true
require 'securerandom'

class Project
  attr_reader :path

  def initialize(path = Pathname.pwd.join('tmp').join(SecureRandom.uuid))
    FileUtils.mkdir_p(path)
    @path = Pathname(path)
  end

  def report_for(type:)
    case type&.to_sym
    when :dependency_scanning
      DependencyScanningReport.new(project_path: path)
    else
      JSON.parse(path.join("gl-#{type}-report.json").read)
    end
  end

  def mount(dir:)
    FileUtils.cp_r("#{dir}/.", path)
  end

  def chdir
    Dir.chdir path do
      yield
    end
  end

  def clone(repo, branch: 'master')
    if branch.match?(/\b[0-9a-f]{5,40}\b/)
      execute({}, 'git', 'clone', repo, path.to_s)
      chdir do
        execute({}, 'git', 'checkout', branch)
      end
    else
      execute({}, 'git', 'clone', '--depth=1', '--single-branch', '--branch', branch, repo, path.to_s)
    end
  end

  def execute(env = {}, *args)
    Bundler.with_unbundled_env do
      system(env, *args, exception: true)
    end
  end

  def cleanup
    FileUtils.rm_rf(path) if path.exist?
  end
end
