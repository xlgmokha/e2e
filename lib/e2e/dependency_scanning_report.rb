# frozen_string_literal: true

class DependencyScanningReport
  def initialize(project_path:)
    report_path = project_path.join('gl-dependency-scanning-report.json')
    @content = report_path.exist? ? JSON.parse(report_path.read) : {}
  end

  def to_h
    @content
  end

  def to_s
    JSON.pretty_generate(to_h)
  end
end
