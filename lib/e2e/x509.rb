# frozen_string_literal: true
require 'openssl'

class X509
  def self.self_signed(key: OpenSSL::PKey::RSA.new(4096))
    certificate = OpenSSL::X509::Certificate.new
    certificate.subject = certificate.issuer = OpenSSL::X509::Name.parse("/C=/ST=/L=/O=/OU=/CN=")
    certificate.not_before = Time.now.to_i
    certificate.not_after = Time.now.to_i + 600
    certificate.public_key = key.public_key
    certificate.serial = 0x01
    certificate.version = 2
    certificate.sign(key, OpenSSL::Digest::SHA256.new)
    certificate
  end
end
