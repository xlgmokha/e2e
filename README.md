# e2e - End to end test suite for security analyzers

## Why?

To develop and test analyzers in isolation for faster feedback cycles.

## Getting Started

You will need the following tools installed:

* [Docker][docker]
* [asdf][asdf]
* [git][git]

This project uses sub modules to track the many different
security product repositories.

To get the project bootstrapped:

```bash
$ git clone --recursive git@gitlab.com:xlgmokha/e2e.git
$ cd e2e
$ asdf install
$ ./bin/setup
```

To run the full test suite:

```bash
$ ./bin/test
```

To run a specific test:

```bash
$ ./bin/test spec/integration/gemnasium_maven_spec.rb:14
```

Export `DEBUG=true` to debug a specific test by launching a shell in the Docker container:

```bash
$ DEBUG=true ./bin/test spec/integration/gemnasium_maven_spec.rb:14
```

## How does this work?

This projects stores a reference to different analyser repositories in the `src`
directory. When tests run, the test suite builds a Docker image from source
then launches a Docker container to run a single test. The `spec/fixtures/` folder
is meant to contain a set of example projects that can be used for testing
many different scenarios to ensure that the output of the analyzer matches the
contract specified in the report schemas.

The suite runs something like the following:

```ruby
docker = Docker.new(Pathname.pwd.join('src/gemnasium-maven'))
docker.build(tag: 'gemnasium-maven:latest')

project = Project.new
project.mount(dir: Pathname.pwd.join('spec/fixtures/java/my-java-project'))

docker.run(image: 'gemnasium-maven:latest', project_path: project.path)
report = project.report_for(type: :dependency_scanning)

expect(report).to match_schema(:dependency_scanning)
```

Happy hacking!

[asdf]: https://asdf-vm.com/
[docker]: https://www.docker.com/
[git]: https://git-scm.com/
